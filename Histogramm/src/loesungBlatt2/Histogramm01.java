// $Id: Histogramm01.java 798 2014-10-02 19:56:47Z wimalopaan $

package loesungBlatt2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Histogramm01 {
	private static final int WRONG_ARGUMENTS_EXITCODE = 1;
	private static final int FILE_NOT_READABLE_EXITCODE = 2;
	private static final int FILE_NOT_FOUND_EXITCODE = 3;
	
	public static void usage() {
		System.out.format("usage: java %s <word> [<word>]", Histogramm01.class.getName());
		System.out.format("       java -s <file>", Histogramm01.class.getName());
	}
	public static void main(String[] arguments) {
		//[main
		if (arguments.length < 1) {
			usage();
			System.exit(WRONG_ARGUMENTS_EXITCODE);
		}
		assert arguments.length >= 1 : "wrong number of arguments";
		Histogramm histogramm = new Histogramm(); // <1>
		if ((arguments.length == 2) && (arguments[0].equals("-f"))) { // <2>
			File file = new File(arguments[1]); // <3>
			if (file.canRead()) { // <4>
				try(Scanner scanner = new Scanner(file)) { // <5>
					while(scanner.hasNext()) { // <6>
						for(final char c: scanner.next().toCharArray()) { // <7>
							histogramm.count(c); // <8>
						}
					}
				}
				catch(FileNotFoundException e) { // <9>
					usage();				
					System.exit(FILE_NOT_FOUND_EXITCODE);
				}
			}
			else { 
				usage();				
				System.exit(FILE_NOT_READABLE_EXITCODE);
			}
		}
		else {
			for(String word: arguments) {  // <10>
				for(final char c: word.toCharArray()) { // <11>
					histogramm.count(c); // <12>
				}
			}			
		}
		System.out.println(histogramm); // <13>
		//]
		System.out.println(histogramm.toString(false)); 
	}
}
//[Histogramm 
class Histogramm {
	//[HistogrammCtor
	public Histogramm() {
		mBuckets = new Bucket[NUMBER_OF_BUCKETS];
		mUsedBuckets = 0;
	}
	//]
	//[HistogrammCount
	public void count(char c) {
		if (!Character.isLetter(c)) {
			return;
		}
		for(int i = 0; i < mUsedBuckets; i += 1) {
			if(c == mBuckets[i].character()) { // <1>
				mBuckets[i].increment();
				return;
			}
		}
		assert mUsedBuckets < mBuckets.length : "too much chars";
		mBuckets[mUsedBuckets] = new Bucket(c); // <2>
		mUsedBuckets += 1;
	}
	//]
	//[HistogrammToString
	@Override
	public String toString() {
		return toString(true);
	}
	public String toString(boolean absolute) {
		String s = "Histogramm {";
		if (absolute) {
			for(int i = 0; i < mUsedBuckets; i += 1) {
				s += mBuckets[i];
			}
		}
		else {
			long totalCount = 0;
			for(int i = 0; i < mUsedBuckets; i += 1) {
				totalCount += mBuckets[i].count();
			}
			for(int i = 0; i < mUsedBuckets; i += 1) {
				assert totalCount > 0;
				s += "[" + mBuckets[i].character() + " " + (double)mBuckets[i].count() / totalCount + "]";
			}
		}
		s += "}\n";
		return s;
	}
	//]
	//[HistogrammAttributes
	private Bucket[] mBuckets; 
	private int mUsedBuckets; // <1>
	private static int NUMBER_OF_BUCKETS = 52; // <2>
	//]
}
//]
//[Bucket
class Bucket {
	public Bucket(char c) {
		mCharacter = c;
		mCount = 1;
	}
	public void increment() {
		mCount += 1;
	}
	public char character() {
		return mCharacter;
	}
	public long count() {
		return mCount;
	}
	@Override
	public String toString() {
		return "[" + mCharacter + ": " + mCount + "]";
	}
	private long mCount;
	final private char mCharacter;
}
//]