package statistics02;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Histogramm01 {
	private static final int WRONG_ARGUMENTS_EXITCODE = 1;
	private static final int FILE_NOT_READABLE_EXITCODE = 2;
	private static final int FILE_NOT_FOUND_EXITCODE = 3;

	public static void usage() {
		System.out.format("usage: java %s <word> [<word>]", Histogramm01.class.getName());
		System.out.format("       java -s <file>", Histogramm01.class.getName());
	}

	public static void main(String[] arguments) {
		// [main
		if (arguments.length < 1) {
			usage();
			System.exit(WRONG_ARGUMENTS_EXITCODE);
		}
		assert arguments.length >= 1 : "wrong number of arguments";
		Histogramm histogramm = new Histogramm(); // <1>
		if ((arguments.length == 2) && (arguments[0].equals("-f"))) { // <2>
			File file = new File(arguments[1]); // <3>
			if (file.canRead()) { // <4>
				try (Scanner scanner = new Scanner(file)) { // <5>
					while (scanner.hasNext()) { // <6>
						for (final char c : scanner.next().toCharArray()) { // <7>
							histogramm.count(c); // <8>
						}
					}
				} catch (FileNotFoundException e) { // <9>
					usage();
					System.exit(FILE_NOT_FOUND_EXITCODE);
				}
			} else {
				usage();
				System.exit(FILE_NOT_READABLE_EXITCODE);
			}
		} else {
			for (String word : arguments) { // <10>
				for (final char c : word.toCharArray()) { // <11>
					histogramm.count(c); // <12>
				}
			}
		}
		System.out.println(histogramm); // <13>

		System.out.println(histogramm.toString(false));
	}
}

class Histogramm {
	public Histogramm() {
		mBuckets = new Bucket[NUMBER_OF_BUCKETS];
		mUsedBuckets = 0;
	}
	
	private class Bucket {
		public Bucket(char c) {
			mCharacter = c;
			mCount = 1;
		}

		@Override
		public String toString() {
			return "[" + mCharacter + ": " + mCount + "]\n";
		}
		
		@Override
		public boolean equals (Object other){
			if (other == this){
				return true;
			}
			if ((other == null) || (getClass() != other.getClass())){
				return false;
			}
			assert (other instanceof Bucket) : "other must be Bucket";
			Bucket b = (Bucket) other;
			return (this.mCharacter == b.mCharacter);
		}
		
		public boolean equals (char c){
			return (mCharacter == c);
		}
		
		@Override
		public int hashCode (){
			Character c = (Character) mCharacter;
			return  c.charValue();
		}

		public long mCount;
		final public char mCharacter;
	}

	public void count(char c) {
		if (!Character.isLetter(c)) {
			return;
		}
		for (int i = 0; i < mUsedBuckets; i += 1) {
			if (mBuckets[i].equals(c)) {  //using overloaded equals() function
				mBuckets[i].mCount ++;
				return;
			}
		}
		assert mUsedBuckets < mBuckets.length : "too much chars";
		mBuckets[mUsedBuckets] = new Bucket(c); // <2>
		mUsedBuckets += 1;
		
	}

	@Override
	public String toString() {
		return toString(true);
	}

	public String toString(boolean absolute) {
		String s = "Histogramm {\n";
		if (absolute) {
			for (int i = 0; i < mUsedBuckets; i += 1) {
				s += mBuckets[i];
			}
		} else {
			long totalCount = 0;
			for (int i = 0; i < mUsedBuckets; i += 1) {
				totalCount += mBuckets[i].mCount;
			}
			for (int i = 0; i < mUsedBuckets; i += 1) {
				assert totalCount > 0;
				s += "[" + mBuckets[i].mCharacter + " " + (double) mBuckets[i].mCount / totalCount + "]\n";
			}
		}
		s += "}";
		return s;
	}

	private Bucket[] mBuckets;
	private int mUsedBuckets; // <1>
	private static int NUMBER_OF_BUCKETS = 52; // <2>


}

