package statistics01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Histogramm01 {
	private static final int WRONG_ARGUMENTS_EXITCODE = 1;
	private static final int FILE_NOT_READABLE_EXITCODE = 2;
	private static final int FILE_NOT_FOUND_EXITCODE = 3;

	public static void usage() {
		System.out.format("usage: java %s <word> [<word>]", Histogramm01.class.getName());
		System.out.format("       java -s <file>", Histogramm01.class.getName());
	}

	public static void main(String[] arguments) {
		// [main
		if (arguments.length < 1) {
			usage();
			System.exit(WRONG_ARGUMENTS_EXITCODE);
		}
		assert arguments.length >= 1 : "wrong number of arguments";
		Histogramm histogramm = new Histogramm(); // <1>
		if ((arguments.length == 2) && (arguments[0].equals("-f"))) { // <2>
			File file = new File(arguments[1]); // <3>
			if (file.canRead()) { // <4>
				try (Scanner scanner = new Scanner(file)) { // <5>
					while (scanner.hasNext()) { // <6>
						for (final char c : scanner.next().toCharArray()) { // <7>
							histogramm.count(c); // <8>
						}
					}
				} catch (FileNotFoundException e) { // <9>
					usage();
					System.exit(FILE_NOT_FOUND_EXITCODE);
				}
			} else {
				usage();
				System.exit(FILE_NOT_READABLE_EXITCODE);
			}
		} else {
			for (String word : arguments) { // <10>
				for (final char c : word.toCharArray()) { // <11>
					histogramm.count(c); // <12>
				}
			}
		}
		System.out.println(histogramm); // <13>

		System.out.println(histogramm.toString(false));
	}
}

class Histogramm {
	public Histogramm() {
		mBuckets = new Bucket[NUMBER_OF_BUCKETS];
		mUsedBuckets = 0;
	}
	
	private class Bucket {
		/*Bucket als Unterklasse von Histogramm:
		 * als private deklariert: Es ist kein Zugriff von außen möglich
		 *    (außerhalb von Histogramm)
		 *    
		 * - es kann auf private Variablen verzichtet werden, statt 
		 *   dessen können die Variablen direkt verändert werden
		 *   
		 * - Vorteil: kürzerer und lesbarer Code durch direkten Zugriff
		 *   (Verzicht auf Funktionsaufrufe)
		 *   
		 * - Nachteil: Klasse Bucket kann nicht mehr einfach für andere 
		 *   Projekte übernommen werden, da in Histogramm eingebettet
		 *   
		 * 
		 * - Geschachtelte Klassen sind genauso benutzbar wie Top-Level-
		 *   Klassen (mit Präfix der umgebenden Klasse). Sie werden als
		 *   static deklariert und werden nur zum Zweck der Strukturierung
		 *   eingesetzt.
		 *   
		 * - Innere Klassen sind der Oberbegriff für alle inneren Klassen in
		 *   Java: Geschachtelte Klassen, Elementklassen, lokale Klassen und
		 *   anonyme innnere Klassen.
	    */
		
		public Bucket(char c) {
			mCharacter = c;
			mCount = 1;
		}

		@Override
		public String toString() {
			return "[" + mCharacter + ": " + mCount + "]\n";
		}

		public long mCount;
		final public char mCharacter;
	}

	public void count(char c) {
		if (!Character.isLetter(c)) {
			return;
		}
		for (int i = 0; i < mUsedBuckets; i += 1) {
			if (c == mBuckets[i].mCharacter) { // <1>
				mBuckets[i].mCount ++;
				return;
			}
		}
		assert mUsedBuckets < mBuckets.length : "too much chars";
		mBuckets[mUsedBuckets] = new Bucket(c); // <2>
		mUsedBuckets += 1;
		
	}

	@Override
	public String toString() {
		return toString(true);
	}

	public String toString(boolean absolute) {
		String s = "Histogramm {\n";
		if (absolute) {
			for (int i = 0; i < mUsedBuckets; i += 1) {
				s += mBuckets[i];
			}
		} else {
			long totalCount = 0;
			for (int i = 0; i < mUsedBuckets; i += 1) {
				totalCount += mBuckets[i].mCount;
			}
			for (int i = 0; i < mUsedBuckets; i += 1) {
				assert totalCount > 0;
				s += "[" + mBuckets[i].mCharacter + " " + (double) mBuckets[i].mCount / totalCount + "]\n";
			}
		}
		s += "}";
		return s;
	}

	private Bucket[] mBuckets;
	private int mUsedBuckets; // <1>
	private static int NUMBER_OF_BUCKETS = 52; // <2>


}
