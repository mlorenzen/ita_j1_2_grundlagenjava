package coffee02;

public class Aufgabe_222 {

	public static void main(String[] args) {
		CoffeeMachine cm = new CoffeeMachine();

		cm.fillWater(CoffeeMachine.WATER_CAPACITY_ml);
		cm.fillBeans(CoffeeMachine.BEANS_CAPACITY_g);

		while (!cm.isEmpty()) {
		    Mug myMug = new Mug(125);
		    cm.fillMug(myMug);
		    cm.setNextFillAmount(118);
			System.out.format("%s, %s \n", myMug, cm);
		}

	}

}

enum CupSize {
	S, M, L
};

class CoffeeMachine {
	public CoffeeMachine() {
		waterLevel = 0.0;
		beansLevel = 0.0;
		nextFillAmount = 50.0;
		nextBeansAmount = 12.5;

		// define cup sizes
		waterPerCup[CupSize.S.ordinal()] = 75;
		waterPerCup[CupSize.M.ordinal()] = 125;
		waterPerCup[CupSize.L.ordinal()] = 200;
		beansPerCup[CupSize.S.ordinal()] = 15;
		beansPerCup[CupSize.M.ordinal()] = 40;
		beansPerCup[CupSize.L.ordinal()] = 65;
	}

	public static final double WATER_CAPACITY_ml = 2000;
	public static final double BEANS_CAPACITY_g = 1000;

	private double waterLevel;
	private double beansLevel;
	private double nextFillAmount;
	private double nextBeansAmount;

	private static final double[] waterPerCup = new double[CupSize.values().length];
	private static final double[] beansPerCup = new double[CupSize.values().length];

	public void fillWater(double water) {
		assert water >= 0 : "Water to be filled below 0";
		waterLevel += water;
		assert waterLevel <= WATER_CAPACITY_ml : "To much water -> spill over!";
		assert waterLevel >= 0 : "Water below 0";
	}

	public void fillBeans(double beans) {
		assert beans >= 0 : "Beans to be filled below 0";
		beansLevel += beans;
		assert beansLevel <= BEANS_CAPACITY_g : "To much beans!";
		assert beansLevel >= 0 : "Beans below 0";
	}

	public boolean isEmpty() {
		if ((waterLevel < waterPerCup[CupSize.S.ordinal()]) || (beansLevel < beansPerCup[CupSize.S.ordinal()])) {
			return true;
		} else {
			return false;
		}

	}

	public double getCoffee(CupSize cup) {
		double coffeeServed = waterPerCup[cup.ordinal()];
		double beansUsed = beansPerCup[cup.ordinal()];
		if (coffeeServed < waterLevel) {
			waterLevel -= coffeeServed;
		} else {
			coffeeServed = waterLevel;
			waterLevel = 0;
		}
		if (beansUsed < beansLevel) {
			beansLevel -= beansUsed;
		} else {
			beansUsed = beansLevel;
			beansLevel = 0;
		}

		assert waterLevel >= 0 : "Water below 0";
		assert beansLevel >= 0 : "Beans below 0";

		return coffeeServed;
	}

	public void setNextFillAmount(double coffeeSize) {
		assert coffeeSize >= 0 : "FillAmount below 0";
		nextFillAmount = coffeeSize;
		nextBeansAmount = coffeeSize / 4D;
	}
	
	public void fillMug(Mug mug){
	    mug.fill(nextFillAmount);
        waterLevel = (waterLevel - nextFillAmount) < 0 ? 0 : (waterLevel - nextFillAmount);
        beansLevel = (beansLevel - nextBeansAmount) < 0 ? 0 : (beansLevel - nextFillAmount);
		assert waterLevel >= 0 : "Water below 0";
		assert beansLevel >= 0 : "Beans below 0";
	}
	
	@Override
	public String toString(){
	    return "[CoffeeMachine: water level = " + waterLevel + "ml; bean stock = " + beansLevel + "g]";
	}

}

class Mug {
    public Mug(double capacity, double fillLevel){
		assert capacity > 0 : "Capacity must be above 0";        
		this.capacity = capacity;
		this.fillLevel = fillLevel;
		assert capacity <= fillLevel : "FillLevel above capacity";
		assert fillLevel>=0:"FillLevel below 0";		
    }
	public Mug(double capacity) {
	    this(capacity, 0.0);
	}

	public Mug() {
		this(150.0, 0.0);
	}
	
	public void fill(double coffeeInMl){
	    assert coffeeInMl >= 0 : "Amount of coffee below 0";
	    fillLevel = Math.min(capacity, fillLevel + coffeeInMl);
	}

	private final double capacity;
	private double fillLevel;
	
	@Override
	public String toString(){
	    return "[Mug: size: " + capacity + "; fill level: " + fillLevel + ".]";
	}

}
