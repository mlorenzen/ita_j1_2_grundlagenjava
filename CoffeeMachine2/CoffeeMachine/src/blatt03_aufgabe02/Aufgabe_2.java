package blatt03_aufgabe02;
//Aufgabenblatt 3


//Aufgabe 2
//Michael Lorenzen

public class Aufgabe_2 {

	public static void main(String[] args) {
		CoffeeMachine cm = new CoffeeMachine();

		cm.fillStorage(new Water(CoffeeMachine.WATER_CAPACITY_ml));
		cm.fillStorage(new Beans(CoffeeMachine.BEANS_CAPACITY_g));

		while (!cm.isEmpty()) {
			mug myMug = new mug(125);
			vessel isoMug = new isolatedMug(450, true); // my mug has a cap
			cm.setNextFillAmount(118);
			cm.fillVessel(myMug);
			System.out.format("%s, %s \n", myMug, cm);
			cm.setNextFillAmount(420);
			cm.fillVessel(isoMug);
			System.out.format("%s, %s \n", isoMug, cm);
		}
	}
}

interface DrinkDispenser {
	boolean isEmpty();
	void fillStorage(Ingredients ingredient);
	void fillVessel(vessel fillQuantity);
}

class CoffeeMachine implements DrinkDispenser {
	public CoffeeMachine() {
		waterLevel = 0.0;
		beansLevel = 0.0;
		nextFillAmount = 50.0;
		nextBeansAmount = 12.5;
	}

	public static final double WATER_CAPACITY_ml = 2000;
	public static final double BEANS_CAPACITY_g = 1000;

	private double waterLevel;
	private double beansLevel;
	private double nextFillAmount;
	private double nextBeansAmount;

	public void fillStorage(Ingredients ingredient){
		assert ((ingredient instanceof Water) || (ingredient instanceof Beans)) : "Only fill water or beans";
		double fillAmount = ingredient.amount();
		assert fillAmount >= 0 : "Fill amount below 0";
		if (ingredient instanceof Water){
			waterLevel = Math.min(WATER_CAPACITY_ml, waterLevel + fillAmount); 
		} else {
			beansLevel = Math.min(BEANS_CAPACITY_g, beansLevel + fillAmount);
		}
		assert waterLevel <= WATER_CAPACITY_ml : "waterLevel above capacity";
		assert beansLevel <= BEANS_CAPACITY_g : "beans quantity above capacity";
		assert waterLevel >= 0 : "water level below 0";
		assert beansLevel >= 0 : "beans level below 0";
	}

	public boolean isEmpty() {
		// machine is empty if below 1 ml water or 1g beans
		if ((waterLevel < 1D) || (beansLevel < 1D)) {
			return true;
		} else {
			return false;
		}

	}

	public void setNextFillAmount(double coffeeSize) {
		assert coffeeSize >= 0 : "FillAmount below 0";
		nextFillAmount = coffeeSize;
		nextBeansAmount = coffeeSize / 4D;
	}

	public void fillVessel(vessel vessel) {
		double actualFill = (nextFillAmount > waterLevel) ? waterLevel : nextFillAmount;
		vessel.fillWith(actualFill);
		waterLevel = (waterLevel - nextFillAmount) < 0 ? 0 : (waterLevel - nextFillAmount);
		beansLevel = Math.max(0, beansLevel - nextBeansAmount);
		// beansLevel = (beansLevel - nextBeansAmount) < 0 ? 0 : (beansLevel -
		// nextFillAmount);
		// if ((beansLevel- nextBeansAmount) < 0) System.out.println("Beans: " +
		// beansLevel);
		assert waterLevel >= 0 : "Water below 0";
		assert beansLevel >= 0 : "Beans below 0";
	}

	@Override
	public String toString() {
		return "[CoffeeMachine: water level = " + waterLevel + "ml; bean stock = " + beansLevel + "g]";
	}

}

abstract class Ingredients {
	abstract double amount();
}

class Water extends Ingredients {
	public Water(double waterAmount) {
		w = waterAmount;
	}

	public double amount() {
		return w;
	}

	private double w;

}

class Beans extends Ingredients {
	public Beans(double beansAmount) {
		b = beansAmount;
	}

	public double amount() {
		return b;
	}

	private double b;
}

abstract class vessel {

	vessel(double cap) {
		capacity = cap;
		fillLevel = 0D;
	}
	
	public void fillWith(double filling){
		fillLevel = Math.min(capacity, filling + fillLevel);
	}

	public final double capacity;
	private double fillLevel;

	@Override
	public String toString(){
		return "capacity: " + capacity + "; fillLevel: " + fillLevel;
	}
}

class mug extends vessel {
	mug(double capacity, boolean hasCap){
		super(capacity);
		this.hasCap = hasCap;
	}
	
	mug(double capacity) {
		this(capacity, false);
	}
	
	public final boolean hasCap;
	
	@Override
	public String toString(){
		return super.toString() + "; hasCap: " + hasCap;
	}
}

class glas extends vessel {
	glas(double capacity) {
		super(capacity);
	}
	
}

class isolatedMug extends mug{
	isolatedMug(double capacity, boolean hasCap) {
		super(capacity, hasCap); 
	}
	
	isolatedMug(double capacity){
		this(capacity, false);
	}
}

class closableMug extends mug implements Closable {
	closableMug(double capacity){
		super(capacity, true); //closabelMugs always have a cap
	}
	
	private boolean closed = false;
	
	public boolean isClosed(){
		return closed;
	}
	
	public void close(){
		closed = true;
	}
	
	@Override
	public String toString(){
		return super.toString() + "; closed: " + isClosed();
	}
}

interface Closable {
	boolean isClosed();
	void close();
}
