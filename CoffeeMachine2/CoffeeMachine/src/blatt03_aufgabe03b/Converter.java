package blatt03_aufgabe03b;

public class Converter {

	public static void main(String[] args) {
	
			/* Schreiben Sie ein Programm zur Umrechnung von Zahlen aus einem
			 *  Zahlensystem in ein anderes stellwertiges Zahlensystem. Das 
			 *  Programm soll folgendermaßen benutzbar sein:
			 *  
			 *  java Converter <fromBase> <fromNumber> <toBase>
			 */
		if (!(args.length == 3)){
			printUsageAndExit();
		}
		
		int fromBase = 0;
		int toBase = 0;
		//int numberBase10 = 0;
		
		try {
			fromBase = Integer.parseInt(args[0]);
			toBase = Integer.parseInt(args[2]);
		} catch (NumberFormatException e){
			printUsageAndExit();
		}
		
		Base10Converter converter = new Base10Converter(fromBase, toBase);
		
		if (!converter.isConvertible(args[1])){
			printUsageAndExit();
		}
		
		String result = converter.convert(args[1]);

		System.out.println("Die Zahl " + args[1] + " der Basis " + args[0] + " entspricht " + result + " zur Basis " + args[2]);
	}	

	private static void printUsageAndExit(){
			System.out.println("Usage: java Converter <fromBase> <fromNumber> <toNumber>");
			System.out.println("Values for base must be between 2 and 36.");
			System.exit(1);
	}
}


abstract class AbstractBaseConverter{
	public boolean isValidDigit(char c){
		if (!(allowedChars.indexOf(c) >= 0)){
			return false;
		}
		return true;
	};
	
	public boolean isValidBase(int base){
		if ((base >= 2) && (base <= 36)){
			return true;
		}
		return false;
	}
	
	// using String instead of char[]
	static final String allowedChars = "0123456789abcdefghijklmnopqrstuvwxyz";
}

class ToBase10Converter extends AbstractBaseConverter{
	public ToBase10Converter(int fromBase) {
		mFromBase = fromBase;
		assert isValidBase(fromBase) : "Wrong Base";
	}
	
	public boolean isConvertible(String fromNumber){
		//test : chars in from number must be lower than base and belong to allowed chars
		for (int i = 0; i < fromNumber.length(); i++){
			if ((allowedChars.indexOf(fromNumber.charAt(i)) >= mFromBase) ||
					(allowedChars.indexOf(fromNumber.charAt(i)) < 0)){
				return false;
			} 
		}
		return true;
	}
	
	public int convert(String fromNumber){
		int power = fromNumber.length() - 1;
		int number = 0;
		for (int i = 0; i < fromNumber.length(); i++){
			int actualPower = (int) Math.pow(mFromBase, power - i);
			number += (valueOfDigit(fromNumber.charAt(i)) * actualPower); 
		}
		return number;
	}
	
	private int valueOfDigit(char c){
		assert allowedChars.indexOf(c) >= 0 : "Character not allowed";
		return allowedChars.indexOf(c);
	}
	
	private int mFromBase;
}

class FromBase10Converter extends AbstractBaseConverter{
	public FromBase10Converter(int toBase) {
		mToBase = toBase;
		assert isValidBase(mToBase) : "Wrong base";
	}
	private int mToBase;
	
	public String convert(int numberBase10){
		StringBuffer buffer = new StringBuffer();
		do {
			buffer.insert(0, allowedChars.charAt(numberBase10 % mToBase));
			numberBase10 /= mToBase;
		} while (numberBase10 / mToBase > 0);
		buffer.insert(0, allowedChars.charAt(numberBase10 % mToBase));
		return buffer.toString();
	}
	
}

class Base10Converter{
	public Base10Converter(int fromBase, int toBase){
		mFromBase = fromBase;
		mToBase = toBase;
	}
	
	public String convert(String number){
		ToBase10Converter fromConverter = new ToBase10Converter(mFromBase);
		FromBase10Converter toConverter = new FromBase10Converter(mToBase);
		return toConverter.convert(fromConverter.convert(number));
	}
	
	public boolean isConvertible(String fromNumber){
		ToBase10Converter fromConverter = new ToBase10Converter(mFromBase);
		return fromConverter.isConvertible(fromNumber);
	}
	
	private int mFromBase, mToBase ;
	
	
}