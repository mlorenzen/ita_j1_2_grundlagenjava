package blatt03_aufgabe01b;


public class Aufgabe_1b {

	public static void main(String[] args) {

	}

}

interface fillalbleObject {
	void fillWith(double filling);
}

abstract class vessel implements fillalbleObject {

	vessel(double cap) {
		capacity = cap;
		fillLevel = 0D;
	}
	
	public void fillWith(double filling){
		fillLevel = Math.min(capacity, filling + fillLevel);
	}

	public final double capacity;
	private double fillLevel;

}

class mug extends vessel {
	mug(double capacity, boolean hasCap){
		super(capacity);
		this.hasCap = hasCap;
	}
	
	mug(double capacity) {
		this(capacity, false);
	}
	
	public final boolean hasCap;
}

class glas extends vessel {
	glas(double capacity) {
		super(capacity);
	}
	
}

class isolatedMug extends mug{
	isolatedMug(double capacity) {
		super(capacity, true); // isolatedMugs always have a cap
	}
}