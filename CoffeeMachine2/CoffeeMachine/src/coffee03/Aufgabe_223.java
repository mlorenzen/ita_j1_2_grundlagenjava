package coffee03;


public class Aufgabe_223 {

	public static void main(String[] args) {
		CoffeeMachine cm = new CoffeeMachine();

		cm.fill(new Water(CoffeeMachine.WATER_CAPACITY_ml));
		cm.fill(new Beans(CoffeeMachine.BEANS_CAPACITY_g));

		while (!cm.isEmpty()) {
			Mug myMug = new Mug(125);
			cm.setNextFillAmount(118);
			cm.fillMug(myMug);
			System.out.format("%s, %s \n", myMug, cm);
		}

	}

}

enum CupSize {
	S, M, L
};

class CoffeeMachine {
	public CoffeeMachine() {
		waterLevel = 0.0;
		beansLevel = 0.0;
		nextFillAmount = 50.0;
		nextBeansAmount = 12.5;
	}

	public static final double WATER_CAPACITY_ml = 2000;
	public static final double BEANS_CAPACITY_g = 1000;

	private double waterLevel;
	private double beansLevel;
	private double nextFillAmount;
	private double nextBeansAmount;


	public void fill(Water water) {
		double fillAmount = water.amount();
		assert fillAmount >= 0 : "Water to be filled below 0";
		waterLevel += fillAmount;
		assert fillAmount <= WATER_CAPACITY_ml : "To much water -> spill over!";
		assert fillAmount >= 0 : "Water below 0";
	}

	public void fill(Beans beans) {
		double fillAmount = beans.amount();
		assert fillAmount >= 0 : "Beans to be filled below 0";
		beansLevel += beans.amount();
		assert beansLevel <= BEANS_CAPACITY_g : "To much beans!";
		assert beansLevel >= 0 : "Beans below 0";
	}

	public boolean isEmpty() {
		// machine is empty if below 1 ml water or 1g beans
		if ((waterLevel < 1D ) || (beansLevel < 1D)) {
			return true;
		} else {
			return false;
		}

	}

	public void setNextFillAmount(double coffeeSize) {
		assert coffeeSize >= 0 : "FillAmount below 0";
		nextFillAmount = coffeeSize;
		nextBeansAmount = coffeeSize / 4D;
	}

	public void fillMug(Mug mug) {
		double actualFill = (nextFillAmount > waterLevel) ? waterLevel : nextFillAmount;
		mug.fill(actualFill);
		waterLevel = (waterLevel - nextFillAmount) < 0 ? 0 : (waterLevel - nextFillAmount);
		beansLevel = Math.max(0, beansLevel - nextBeansAmount);
		//beansLevel = (beansLevel - nextBeansAmount) < 0 ? 0 : (beansLevel - nextFillAmount);
		//if ((beansLevel- nextBeansAmount) < 0) System.out.println("Beans: " + beansLevel);
		assert waterLevel >= 0 : "Water below 0";
		assert beansLevel >= 0 : "Beans below 0";
	}

	@Override
	public String toString() {
		return "[CoffeeMachine: water level = " + waterLevel + "ml; bean stock = " + beansLevel + "g]";
	}

}

class Water {
	public Water(double waterAmount) {
		w = waterAmount;
	}

	public double amount() {
		return w;
	}

	private double w;

}

class Beans {
	public Beans(double beansAmount) {
		b = beansAmount;
	}

	public double amount() {
		return b;
	}

	private double b;
}

	class Mug {
		public Mug(double capacity, double fillLevel) {
			assert capacity > 0 : "Capacity must be above 0";
			this.capacity = capacity;
			this.fillLevel = fillLevel;
			assert capacity >= fillLevel : "FillLevel above capacity";
			assert fillLevel >= 0 : "FillLevel below 0";
		}

		public Mug(double capacity) {
			this(capacity, 0.0);
		}

		public Mug() {
			this(150.0, 0.0);
		}

		public void fill(double coffeeInMl) {
			assert coffeeInMl >= 0 : "Amount of coffee below 0";
			fillLevel = Math.min(capacity, fillLevel + coffeeInMl);
		}

		private final double capacity;
		private double fillLevel;

		@Override
		public String toString() {
			return "[Mug: size: " + capacity + "; fill level: " + fillLevel + ".]";
		}

	}
