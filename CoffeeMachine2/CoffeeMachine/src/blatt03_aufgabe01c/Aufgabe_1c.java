package blatt03_aufgabe01c;

//ml: mir war leider nicht klar, welches Fehlverhalten ich darstellen sollte

public class Aufgabe_1c {

	public static void main(String[] args) {
		vessel[] vessels = new vessel[4];
		vessels[0] = new mug(150);
		vessels[1] = new glas(500);
		vessels[2] = new isolatedMug(300, true);
		vessels[3] = new closableMug(250);
		
		for (vessel myVessel: vessels){
			System.out.println(myVessel);
		}

	}

}

interface fillalbleObject {
	void fillWith(double filling);
}

interface Closable {
	boolean isClosed();
	void close();
}

abstract class vessel implements fillalbleObject {

	vessel(double cap) {
		capacity = cap;
		fillLevel = 0D;
	}
	
	public void fillWith(double filling){
		fillLevel = Math.min(capacity, filling + fillLevel);
	}

	public final double capacity;
	private double fillLevel;

	@Override
	public String toString(){
		return "capacity: " + capacity + "; fillLevel: " + fillLevel;
	}
}

class mug extends vessel {
	mug(double capacity, boolean hasCap){
		super(capacity);
		this.hasCap = hasCap;
	}
	
	mug(double capacity) {
		this(capacity, false);
	}
	
	public final boolean hasCap;
	
	@Override
	public String toString(){
		return super.toString() + "; hasCap: " + hasCap;
	}
}

class glas extends vessel {
	glas(double capacity) {
		super(capacity);
	}
	
}

class isolatedMug extends mug{
	isolatedMug(double capacity, boolean hasCap) {
		super(capacity, hasCap); 
	}
	
	isolatedMug(double capacity){
		this(capacity, false);
	}
}

class closableMug extends mug implements Closable {
	closableMug(double capacity){
		super(capacity, true); //closabelMugs always have a cap
	}
	
	private boolean closed = false;
	
	public boolean isClosed(){
		return closed;
	}
	
	public void close(){
		closed = true;
	}
	
	@Override
	public String toString(){
		return super.toString() + "; closed: " + isClosed();
	}
}