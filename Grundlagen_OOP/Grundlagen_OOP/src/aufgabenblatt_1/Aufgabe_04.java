package aufgabenblatt_1;


public class Aufgabe_04 {

	public static void main(String[] args) {
		/* Schreiben Sie ein Programm zur Umrechnung von Zahlen aus einem
		 *  Zahlensystem in ein anderes stellwertiges Zahlensystem. Das 
		 *  Programm soll folgendermaßen benutzbar sein:
		 *  
		 *  java Converter <fromBase> <fromNumber> <toBase>
		 */
		String erlaubteZeichen = "0123456789abcdefghijklmnopqrstuvwxyz";

		if (!eingabenOk(args, erlaubteZeichen)) {
			System.out.println("Bitte Argumente folgendermaßen angeben:");
			System.out.println("<fromBase> <fromNumber> <toBase>");
			return;
		}
		
		
		// Eingabe in Basis 10 umrechnen
		int inputBase10 = toBase10(args[0], args[1], erlaubteZeichen);
		
		// Zahl in Basis 10 auf ZielBasis umrechnen
		String ergebnis = toNewBase(inputBase10, args[2], erlaubteZeichen);
		
		System.out.println("Die Zahl " + args[1] + " der Basis " + args[0] + " entspricht " + ergebnis + " zur Basis " + args[2]);
		
	}
	
	private static String toNewBase(int zahlBase10, String newBase, String zeichenKette){
		StringBuffer ergebnis = new StringBuffer();
		int basis = Integer.parseInt(newBase);
		do {
			ergebnis.insert(0, zeichenKette.charAt(zahlBase10 % basis));
			zahlBase10 /= basis;
		} while (zahlBase10 / basis > 0);
			ergebnis.insert(0, zeichenKette.charAt(zahlBase10 % basis));
		return ergebnis.toString();
	}
	
	private static int toBase10(String fromBase, String fromNumber, String zeichenWerte) {
		int potenz = fromNumber.length() - 1;
		int zahlBase10 = 0;
		for (int i = 0; i < fromNumber.length(); i++){
			int wert = getWert(fromNumber.charAt(i), zeichenWerte);
			int rundenPotenz = (int) Math.pow(Integer.parseInt(fromBase), (potenz - i));
			zahlBase10 += wert * rundenPotenz;
		}
		return zahlBase10;
	}
	
	private static int getWert(char zeichen, String zeichenkette){
		return zeichenkette.indexOf(zeichen);
	}

 	private static boolean eingabenOk(String[] argumente, String testZeichen) {
		// Test: Es müssen genaue 3 Argumente angegeben werden
		if (argumente.length != 3) return false;

		// Test: Erlaubt sind nur Basiswerte zwischen 2 und 36
		if ((Integer.parseInt(argumente[0]) < 2) || (Integer.parseInt(argumente[0]) > 36)) return false;
		if ((Integer.parseInt(argumente[2]) < 2) || (Integer.parseInt(argumente[2]) > 36)) return false;
		
		// Test: <fromNumber> darf nur Zeichen bis max. <fromBase> enthalten
		for (int i = 0; i < argumente[1].length(); i++){
			if (testZeichen.indexOf(argumente[1].charAt(i)) >= Integer.parseInt(argumente[0])) return false;
		}
		
		// Alle Tests bestanden
		return true;
	}
}

