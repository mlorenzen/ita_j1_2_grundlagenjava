package aufgabenblatt_1;

public class Aufgabe_03 {

	public static void main(String[] args) {
		/*Verbessern Sie Ihre Lösung nochmals, indem Sie versuchen reguläre Ausdrücke
		 *  an geeigneter Stelle einzusetzen. 
		 *  Erläutern Sie auch die Bildung des notwendigen regulären Ausdrucks.
		 */

		if (args.length == 0) {
			System.out.println("Keine Argumente angegeben");
			return;
		}
		
		if (isInteger(args[0])){
		
		int sekunden = Integer.parseInt(args[0]);
		int minuten = sekunden / 60;
		int restlicheSekungen = sekunden % 60;
		int stunden = minuten / 60;
		int restlicheMinuten = minuten % 60;
			
		System.out.printf("Die Eingabe entspricht %d Stunde(n), %d Minute(n) und %d Sekunde(n).%n", stunden, restlicheMinuten, restlicheSekungen);
		} else {
			System.out.println("Es wurde keine Integerzahl übergeben");
		}

	}
	
	protected static boolean isInteger(String testString){
		// regulärer Ausdruck erlaubt nur Vorzeichen (nicht zwingend) und 
		// mindestens eine Ziffer
		boolean b = testString.matches("[+-]?[0-9]+");
		
		// Test 1: besteht String nur aus Zahlen oder Vorzeichen
		if (!b) return false;
		
		//Test 2: ist Wert zu groß für Integer
		long zahl = Long.parseLong(testString);
		if ((zahl > Integer.MAX_VALUE) || (zahl < Integer.MIN_VALUE)) return false;
		
		// Alle Tests bestanden
		return true;
	}

}
