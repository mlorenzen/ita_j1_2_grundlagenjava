package aufgabenblatt_1;


public class Aufgabe_06 {
/* Gegeben war folgender Code
 * public static void main(String[] arguments) {
 *  System.out.println(foo(1, 2, 3, 4, 5));
 *}
 *private static int foo(int... x) {
 *  return x.length == 0 ? 0 : foo(Arrays.copyOf(x, x.length - 1)) + x[x.length - 1];
 *}
 *
 * Ergebnis nach review:
 */
	public static void main(String[] args) {
		System.out.println(sumIntegers(1, 2, 3, 4, 5));

	}

	private static int sumIntegers(int... numbers) {
		int result = 0;
		for (int currentNumber : numbers){
			result += currentNumber;
		}
		return result;

	}

}
