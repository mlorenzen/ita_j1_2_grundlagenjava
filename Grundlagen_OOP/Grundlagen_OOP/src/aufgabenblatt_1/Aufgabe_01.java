package aufgabenblatt_1;

public class Aufgabe_01 {

	public static void main(String[] args) {
//		Schreiben Sie ein Programm, dass eine Anzahl von Sekunden als ersten Parameter
//		der Kommandozeile einliest, und in die Anzahl Stunden, Minuten und Sekunden umrechnet. 
//		Testen Sie Ihr Programm mit Werten von 0, 59, 61, 301, 3600, 3663, etc. 
//		Was passiert bei einem Wert von -1. Was geschieht bei 1e03.
		
		int sekunden = Integer.parseInt(args[0]);
		int minuten = sekunden / 60;
		int restlicheSekungen = sekunden % 60;
		int stunden = minuten / 60;
		int restlicheMinuten = minuten % 60;
			
		System.out.printf("Die Eingabe entspricht %d Stunde(n), %d Minute(n) und %d Sekunde(n).%n", stunden, restlicheMinuten, restlicheSekungen);

	}
	

}
