package aufgabenblatt_1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Aufgabe_05b {
	public static void main(String[] args){
		int iterationen = 5;
		
		File file = new File(args[0]);
		if (!file.canRead()) {
			System.out.println("Bitte einen existierenden Dateinamen angeben.");
			System.exit(1);
		}
		
		
		boolean[][] cells = readCells(file);

		System.out.println("lebende Zelle: o; tote Zelle: x");
		System.out.println("Anfangspopulation:");
		printCells(cells);
		
		for (int i = 1; i <= iterationen; i++){
			boolean[][] newGeneration;
			newGeneration = nextGeneration(cells);
			System.out.println("Die " + i + ". Folgegeneration:");
			printCells(newGeneration);
			cells = newGeneration;
		}

	}
	
	private static boolean[][] readCells(File file){
		boolean[][] arrayIn;
		int lines = 0;
		int cols = 0;
		try (Scanner scanner = new Scanner(file)){
			// Größe des Arrays ermitteln
			while (scanner.hasNextLine()){
				String line = scanner.nextLine();
				lines++;
				cols = line.length();
			}
			arrayIn = new boolean[lines][cols];
		} catch (FileNotFoundException f) {
			return null;
		}
//				scanner.close();
		try (Scanner scanner = new Scanner(file)){
			int lineCounter = 0;
			while (scanner.hasNextLine()){
				String line = scanner.nextLine();
				for (int i = 0; i < line.length(); i++){
					if (line.charAt(i) == 'o') arrayIn[lineCounter][i] = true;
				}
				lineCounter++;
			}
		} catch (FileNotFoundException e){
			return null;
		}
		return arrayIn;
	}
	
	private static boolean[][] nextGeneration(boolean[][] oldGeneration){
		boolean[][] nextGeneration = new boolean[oldGeneration.length][oldGeneration[0].length];
		for (int x = 0; x < oldGeneration.length; x++){
			for (int y = 0; y < oldGeneration[x].length; y++){
				int livingNeighbours = getLivingNeighbours(x, y, oldGeneration);
				if (oldGeneration[x][y]){
					if (livingNeighbours == 2 || livingNeighbours == 3){
						nextGeneration[x][y] = true;
					} 
				} else {
					if (livingNeighbours == 3){
						nextGeneration[x][y] = true;
					}
				}
			}
		}
		return nextGeneration;
	}
	
	private static int getLivingNeighbours(int x, int y,boolean[][] cells){
		int counter = 0;
		for (int i = -1; i <= 1; i++){
			int xPos = x + i;
			for (int j = -1; j <= 1; j++){
				int yPos = y + j;
				if (!(x==xPos && y==yPos)){ // Ausgangszelle nicht bewerten
						// System.out.println("Auswertungsposition" + xPos + ":" + yPos); // debug-Ausgabe
					if (getCellStatus(xPos, yPos, cells.length, cells[x].length, cells)){
						counter++;
					}
				}
			}
		}
		//System.out.println("Lebende Nachbarn für " + x + " - " + y + ": " + counter); // debug-Ausgabe
		return counter;
	}
	
	

	private static void printCells(boolean[][] cells){
		for (boolean[] line: cells){
			for (boolean cell: line){
				System.out.print(cell ? 'o' : 'x' );
				System.out.print(" ");
			}
			System.out.print("\n");
		}
	}

		private static boolean getCellStatus(int x, int y, int xSize, int ySize, boolean[][] aktuellePopulation){
			if (x < 0 || x >= xSize) return false;
			if (y < 0 || y >= ySize) return false;
			return aktuellePopulation[x][y];
		}

}
