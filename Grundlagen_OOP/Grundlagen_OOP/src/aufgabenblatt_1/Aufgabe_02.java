package aufgabenblatt_1;

public class Aufgabe_02 {

	public static void main(String[] args) {
		/* Verbessern Sie die obige Lösung, indem Sie eine Prüfmethode 
		 * (Prädikat) isInteger() schreiben.
		 */
		
		if (isInteger(args[0])){
		
		int sekunden = Integer.parseInt(args[0]);
		int minuten = sekunden / 60;
		int restlicheSekungen = sekunden % 60;
		int stunden = minuten / 60;
		int restlicheMinuten = minuten % 60;
			
		System.out.printf("Die Eingabe entspricht %d Stunde(n), %d Minute(n) und %d Sekunde(n).%n", stunden, restlicheMinuten, restlicheSekungen);
		} else {
			System.out.println("Es wurde keine Integerzahl übergeben");
		}

	}
	
	protected static boolean isInteger(String testString){
		String ziffern = "0123456789";
		
		// Test 1: besteht String nur aus Zahlen
		for (int i = 0; i < testString.length(); i++){
			if (ziffern.indexOf(testString.charAt(i)) < 0) return false;
		}
		
		//Test 2: ist Wert zu groß für Integer
		long zahl = Long.parseLong(testString);
		if ((zahl > Integer.MAX_VALUE) || (zahl < Integer.MIN_VALUE)) return false;
		
		// Alle Tests bestanden
		return true;
	}

}
