package praesenztag;

import java.util.Locale;
import java.util.Calendar;

public class Aufgabe_03 {

	public static void main(String[] args) {
		boolean myBool = true;
		char myChar = 'a';
		byte myByte = 42;
		short myShort = 20000;
		int myInteger = 4505578;
		long myLong = 298776590011111L;
		float myFloat = 45.3f;
		double myDouble = 46.345554D;
		Calendar kalender = Calendar.getInstance();
		
		System.out.println(myBool);
		System.out.println(myChar);
		System.out.println(myByte);
		System.out.println(myShort);
		System.out.println(myInteger);
		System.out.println(myLong);
		System.out.println(myFloat);
		System.out.println(myDouble);

		System.out.format(Locale.GERMAN, "Ja oder Nein %b %n", myBool);
		System.out.format(Locale.GERMAN, "Der Char: %c %n", myChar);
		System.out.format(Locale.GERMAN, "Das Byte: %o %n", myByte);
		System.out.format(Locale.GERMAN, "Hier ein Short: %s %n", myShort);
		System.out.format(Locale.GERMAN, "Integer gibt es auch %,d.%n", myInteger);
		System.out.format(Locale.GERMAN, "Und ganz lang %,d %n", myLong);
		System.out.format(Locale.GERMAN, "Mit Komma, dreizehn Stellen vor und zwei nach dem Komma %13.2f %n", myFloat);
		System.out.format(Locale.GERMAN, "Und doppelt genau %5.3f %n", myDouble);
		System.out.format(Locale.GERMAN, "Das heutige Datum ist der %td. %tB %tY. %n", kalender, kalender, kalender);
		
		System.out.println("Der erste Block sorgt für die Lokalisierung, der zweite den Text, die Positionierung");
		System.out.println("und im letzten Block stehen die auzugebenden Variablen.");
	}

}
