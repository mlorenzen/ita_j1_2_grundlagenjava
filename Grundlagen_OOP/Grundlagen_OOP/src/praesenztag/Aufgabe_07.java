package praesenztag;

public class Aufgabe_07 {

	public static void main(String[] args) {

		int min, max;
		
		if (args.length > 0){
		max = Integer.parseInt(args[0]);
		min = Integer.parseInt(args[0]);
		} else {
			System.out.println("Keine Argumente angegeben.");
			return;
		}
		
		for (String arg: args){
			int var = Integer.parseInt(arg);
			if (var > max) max = var;
			if (var < min) min = var;
		}
		
		System.out.println("Das Maximum ist " + max);
		System.out.println("Das Minimum ist " + min);
	}

}
