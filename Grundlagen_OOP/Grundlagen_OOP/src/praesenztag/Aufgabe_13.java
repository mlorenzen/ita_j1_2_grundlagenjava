package praesenztag;



public class Aufgabe_13 {

	public static void main(String[] args) {
		/* Prüfen Sie für jeden String, ob ein String ausschließlich aus Ziffern besteht.
		 * Unterscheiden Sie positive und negative Zahlen.
		 */
		if (args.length > 0){
			for (String arg: args){
				if (isIntegerParsable(arg)){
					System.out.format("Das Argument %s besteht aus Ziffern und ist %s.%n", arg, bestimmeVorzeichen(arg));
				} else {
					System.out.format("Das Argument %s besteht nicht nur aus Ziffern.%n", arg );
				}
			}
		} else {
			System.out.println("Es wurden keine Argumente übergeben.");
		}

	}
	
	protected static String bestimmeVorzeichen(String argument){
		if (argument.charAt(0) == '-') {
			return "negativ";
		} else {
			return "positiv";
		}
	}
	
	protected static boolean isIntegerParsable(String argument){
		boolean istString = true;
		
		if (!(argument.charAt(0)== '-') && !(argument.charAt(0) == '+') && (!istZiffer(argument.charAt(0)))) istString = false;
		for (int i = 1; i < argument.length(); i++){
			if (!istZiffer(argument.charAt(i))) istString = false; 
		}
		return istString;
	}
	
	protected static boolean istZiffer(char c){
		try {
			int tmp = Integer.parseInt(Character.toString(c));
		} catch (NumberFormatException e){
			return false;
		}
		return true;
	}

}
