package praesenztag;

public class Aufgabe_10 {

	public static void main(String[] args) {
		// Schreiben Sie ein Programm, dass den ersten String, der an main() übergeben wird,
		// als ganze Zahl interpretiert und die Quersumme (Ziffernsumme) berechnet.
		// Eine Behandlung der möglicherweise auftredenden Ausnahmen (Exception) ist nicht notwendig. 

		int zahl = Integer.parseInt(args[0]);
		int gegeben = zahl;
		int quersumme = 0;
		
		do {
			quersumme += zahl % 10;
			zahl /= 10;
		} while (zahl % 10 > 0);
		
		System.out.format("Die Quersumme von %d ist %d.%n", gegeben, quersumme);
	}

}
