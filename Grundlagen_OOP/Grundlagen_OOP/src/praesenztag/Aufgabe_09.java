package praesenztag;

import java.util.Locale;

public class Aufgabe_09 {

	public static void main(String[] args) {
		int zahl = 0;
		zahl = Integer.parseInt(args[0]);
		int zaehler = 1;
		Locale g = Locale.GERMAN;
		System.out.format(g, "Es wird %d behandelt.%n%n", zahl);
		
		while (zahl / 10 > 0){
			System.out.format(g, "Ergebnis für die %,2d. Stelle von rechts: %d%n", zaehler, zahl%10);
			zaehler++;
			zahl /= 10;
		}
		System.out.format(g, "Ergebnis für die %,2d. Stelle von rechts: %d%n%n", zaehler, zahl%10);
		
		System.out.format("Die Zahl hat %d Stellen.%n", zaehler);

	}

}
