package praesenztag;

public class Aufgabe_11 {

	public static void main(String[] args) {
		// Finden Sie heraus, ob ein String ausschließlich aus Ziffern besteht.
		
		boolean istString = true;
		
		for (int i = 0; i < args[0].length(); i++){
			try {
				int x = Integer.parseInt(Character.toString(args[0].charAt(i)));
			} catch (NumberFormatException a) {
				istString = false;
			}
		}
		
		if (istString) {
			System.out.println("Der Eingabewert " + args[0] + " ist eine Integer-Zahl.");
		} else {
			System.out.println("Der Eingabewert " + args[0] + " ist keine Integer-Zahl.");
		}			

	}

}
