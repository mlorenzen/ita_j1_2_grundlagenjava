package praesenztag;

public class Aufgabe_05 {

	public static void main(String[] args) {
		while (testInput(args) == false) {
			System.out.println("Bitte genau vier Zahlen als Argumente übergeben");
			System.out.println("Reihenfolge a, b, c, x. Berechnet wird ax^2 + bx + c.");
			return;
		}
		
		double a, b, c, x, y;
		a = Double.parseDouble(args[0]);
		b = Double.parseDouble(args[1]);
		c = Double.parseDouble(args[2]);
		x = Double.parseDouble(args[3]);
		
		y = a * Math.pow(x, 2) + b * x + c;
		
		System.out.format("Das Ergebnis der Rechnung y=ax^2+bx+c mit a=%.2f, b=%.2f, c=%.2f und x=%.2f ist:%n", a, b, c, x);
		System.out.format("y = %.4f%n", y);
		
		}
	
	protected static boolean testInput(String[] argumente){
		if (argumente.length != 4) return false;
		return true;
	}
}
