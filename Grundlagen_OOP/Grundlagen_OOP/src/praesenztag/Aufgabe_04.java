package praesenztag;

import java.util.Locale;

public class Aufgabe_04 {

	public static void main(String[] args) {
		int zahl = 2768;
		int zaehler = 1;
		Locale g = Locale.GERMAN;
		System.out.format(g, "Es wird %d behandelt.%n%n", zahl);
		
		while (zahl / 10 > 0){
			System.out.format(g, "Ergebnis für die %,2d. Stelle von rechts: %d%n", zaehler, zahl%10);
			zaehler++;
			zahl /= 10;
		}
		System.out.format(g, "Ergebnis für die %,2d. Stelle von rechts: %d%n", zaehler, zahl%10);

	}

}
