package praesenztag;

public class Aufgabe_06 {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		if (a > b){
			System.out.println("Das Maximum ist " + a);
			System.out.println("Das Minimum ist " + b);
		} else {
			System.out.println("Das Maximum ist " + b);
			System.out.println("Das Minimum ist " + a);
		}
	}

}
